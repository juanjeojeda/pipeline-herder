"""Test the matchers."""
import re
import unittest
from unittest import mock

import responses

from pipeline_herder import matchers
from pipeline_herder import matching
from pipeline_herder import settings
from pipeline_herder import utils


class TestChecker(unittest.TestCase):
    """Base class with Gitlab variables."""

    user = {
        "id": 2,
    }

    project_project = {
        "id": "project",
        "path_with_namespace": "project",
    }

    pipeline_524445 = {
        "id": 524445,
        "user": {
            "id": 1,
        }
    }

    variables_524445 = []

    jobs_524445 = [{
        "id": 764828,
        "name": "build x86_64",
        "user": {
            "id": 1,
        },
    }]

    job_764828 = {
        "id": 764828,
        "status": "failed",
        "stage": "build",
        "name": "build x86_64",
        "pipeline": {
            "id": 524445,
        },
        "user": {
            "id": 1,
            "username": "user",
        },
        "web_url": "https://host.name/",
    }

    artifacts_764828 = {}

    base_url = 'https://host/api/v4/projects/project'

    def mock_responses(self):
        responses.add(responses.GET, 'https://host/api/v4/user',
                      json=self.user)
        responses.add(responses.GET, f'{self.base_url}',
                      json=self.project_project)
        responses.add(responses.GET, f'{self.base_url}/jobs/764828',
                      json=self.job_764828)
        responses.add(responses.GET, f'{self.base_url}/jobs/764828/trace',
                      body='\n'.join(self.trace_764828))
        for name, contents in self.artifacts_764828.items():
            mangled_name = name.replace(".", "%2E")
            url = f'{self.base_url}/jobs/764828/artifacts/{mangled_name}'
            responses.add(responses.GET, url, '\n'.join(contents))
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445',
                      json=self.pipeline_524445)
        responses.add(responses.GET,
                      f'{self.base_url}/pipelines/524445/variables',
                      json=self.variables_524445)
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445/jobs',
                      json=self.jobs_524445)


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestCode137(TestChecker):
    """Test the detection of pod resource exhaustion (exit code 137)."""

    trace_764828 = (
        '... snip ...',
        '%_rpmdir /builds/cki-project/cki-pipeline/workdir/rpms',
        'section_end:1586362335:build_script',
        '[0Ksection_start:1586362335:after_script',
        '[0K[0K[36;1mRunning after_script[0;m',
        '[0;msection_end:1586362335:after_script',
        '[0Ksection_start:1586362335:upload_artifacts_on_failure',
        '[0K[0K[36;1mUploading artifacts for failed job[0;m',
        '[0;msection_end:1586362335:upload_artifacts_on_failure',
        '[0K[31;1mERROR: Job failed: command terminated with exit code 137',
        '[0;m',
    )

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_exit_137(self, submit_retry: mock.MagicMock):
        """Test the detection of pod resource exhaustion (exit code 137)."""
        self.mock_responses()

        action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        submit_retry.assert_called_once_with(
            mock.ANY, matchers.MATCHERS[0], 0)


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestForcePush(TestChecker):
    """Test the detection of force push on merge jobs."""

    trace_764828 = (
        '... snip ...',
        'See "git help gc" for manual housekeeping.',
        'Successfully rebased and updated refs/heads/queue/5.7.',
        'fatal: reference is not a tree: d6d92c1d',
        '202e1926e06333ee3dee9fe2c1b5ed78',
        'Running after_script',
        '00:01',
        'Running after script...',
    )

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_force_push(self, submit_retry: mock.MagicMock):
        """Test force push checks."""
        self.mock_responses()

        action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestFallback(TestChecker):
    """Test the detection of force push on merge jobs."""

    trace_764828 = (
        'Something failed',
    )
    matchers = [
        utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            action='something-unhandled',
        )
    ]

    @responses.activate
    @mock.patch.object(matching.matchers, 'MATCHERS', matchers)
    def test_fallback(self):
        """Test fall backing to error action if action is not handled."""
        self.mock_responses()

        action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'error')


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestTooLarge(TestChecker):
    """Test the detection of artifacts size exceeded."""

    trace_764828 = (
        '... snip ...',
        'Uploading artifacts for failed job',
        'Uploading artifacts...',
        'rc: found 1 matching files',
        'artifacts: found 2865 matching files',
        'ERROR: Uploading artifacts to coordinator... too large archive  ',
        'id=931289 responseStatus=413 Request Entity Too Large status=413 ',
        'Request Entity Too Large token=s216cQxY',
        'FATAL: too large',
        'ERROR: Job failed: exit code 2',
    )

    @responses.activate
    def test_artifacts_size(self):
        """Test the detection of artifact size exceeded."""
        self.mock_responses()

        action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'report')


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestGitCache(TestChecker):
    """Test the detection of missing git-caches."""

    trace_764828 = (
        '... snip ...',
        'download failed: s3://cki/git-cache/torvalds.linux.tar to - An error'
        ' occurred (NoSuchKey) when calling the GetObject operation: Unknown',
        'tar: This does not look like a tar archive',
        'tar: Exiting with failure status due to previous errors',
        '... snip ...',
        'ERROR: Job failed: command terminated with exit code 1',
    )

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_git_cache(self, submit_retry: mock.MagicMock):
        """Test the detection of artifact size exceeded."""
        self.mock_responses()

        action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestArtifactsMatching(TestChecker):
    """Test the detection of missing git-caches."""

    trace_764828 = (
        'Something failed',
    )

    artifacts_764828 = {'artifacts/build.log': (
        '00:01:18 /bin/sh: fork: Resource temporarily unavailable',
        '00:01:19   LD [M]  drivers/gpu/drm/vmwgfx/vmwgfx.o',
        '00:01:19 make[3]: *** [scripts/Makefile.build:500: fs] Error 2',
        '... snip ...',
        '00:01:21 make: *** [Makefile:1523: targz-pkg] Error 2',
    )}

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_match(self, submit_retry: mock.MagicMock):
        """Test the detection only for specific jobs."""
        self.mock_responses()

        mock_matchers = [utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            job_name='build',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_nomatch(self, submit_retry: mock.MagicMock):
        """Test the detection if the job name differs."""
        self.mock_responses()

        mock_matchers = [utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            job_name='test',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_artifact(self, submit_retry: mock.MagicMock):
        """Test the detection in the job artifacts."""
        self.mock_responses()

        action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('pipeline_herder.matching.notify_finished', mock.Mock())
class TestRCMatcher(TestChecker):
    """Test the detection of missing keys in rc file."""

    trace_764828 = (
        'Something failed',
    )

    artifacts_764828 = {'rc': (
        '[state]',
        'key_text = some value',
        'key_url = https://gitlab/artifact/foobar',
        '[build]',
        'key_other = foo',
        'key_anything = foo foo bar',
    )}

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_ok(self, submit_retry: mock.MagicMock):
        """Check that the rc file is ok."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/key_text': re.compile(r'.*some value.*'),
                'state/key_url': re.compile(r'https://gitlab/.*'),
                'build/key_other': re.compile(r'foo'),
                'build/key_anything': re.compile(r'.*'),
            },
            job_name='build',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_missing_key(self, submit_retry: mock.MagicMock):
        """Check that the rc file is missing a key."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/key_text': re.compile(r'.*some value.*'),
                'build/key_foo': re.compile(r'.*'),
            },
            job_name='build',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_wrong_value(self, submit_retry: mock.MagicMock):
        """Check that the rc file has a key but the value is wrong."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/key_text': re.compile(r'.*other value.*'),
            },
            job_name='build',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_missing_rc(self, submit_retry: mock.MagicMock):
        """Check that the rc file is missing."""
        self.artifacts_764828 = {}
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/key_text': re.compile(r'.*other value.*'),
            },
            job_name='build',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_different_action(self, submit_retry: mock.MagicMock):
        """Check that a different action is performed if configured to."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/key_text': re.compile(r'.*other value.*'),
            },
            job_name='build',
            action='report',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_check(self, submit_retry: mock.MagicMock):
        """Check that the check correctly handles the job name."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/missing_key': re.compile(r'.*other value.*'),
            },
            job_name='test',
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_check_any(self, submit_retry: mock.MagicMock):
        """Check that the check ignores job name if not set."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={
                'state/missing_key': re.compile(r'.*other value.*'),
            },
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_check_no_check(self, submit_retry: mock.MagicMock):
        """Check that it's possible to check just that the rc file is there."""
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={},
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_job_check_no_check_fail(self, submit_retry: mock.MagicMock):
        """Check that it's possible to check just that the rc file is there."""
        self.artifacts_764828 = {}
        self.mock_responses()

        mock_matchers = [utils.RCMatcher(
            name='error-dummy',
            description='Dummy error',
            checks={},
        )]

        with mock.patch.object(matching.matchers, 'MATCHERS', mock_matchers):
            action = matching.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)
