#!/bin/bash

export START_REDIS=true
export START_CELERY_HERDER="--app pipeline_herder.worker --concurrency 1 --queues herding"
export START_PYTHON_MATCHING=pipeline_herder.matching
export LOG_NAME=pipeline-herder

exec cki_entrypoint.sh
