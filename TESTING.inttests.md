# Integration tests

Job management has integration tests that must be run before merging any changes.

To run them, you need to have access to two accounts on the GitLab instance
normally used for managing pipelines.
Then, define the following environment variables:

- `IT_GITLAB_URL`: `xci32` URL
- `IT_GITLAB_PARENT_PROJECT`: cki-project
- `IT_GITLAB_PERSONAL_TOKEN`: your personal access token
- `IT_GITLAB_CI_BOT_TOKEN`: a personal access token for a CI bot
- `IT_GITLAB_HERDER_CI_BOT_TOKEN`: a personal access token for another CI bot

You can run the integration tests with

```shell
# run all tests
python3 -m unittest inttests.test_herder -v
# run an individual test
python3 -m unittest inttests.test_herder.TestClass.test_method -v
```
